'use strict';

var _requestPromise = require('request-promise');

var _requestPromise2 = _interopRequireDefault(_requestPromise);

var _users = require('./services/users');

var _users2 = _interopRequireDefault(_users);

var _groupChannels = require('./services/groupChannels');

var _groupChannels2 = _interopRequireDefault(_groupChannels);

var _channelMembers = require('./services/channelMembers');

var _channelMembers2 = _interopRequireDefault(_channelMembers);

var _messages = require('./services/messages');

var _messages2 = _interopRequireDefault(_messages);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DEFAULT_BASE_URL = 'https://api.sendbird.com/v3/';

function init(app, _ref) {
  var apiToken = _ref.apiToken,
      _ref$baseUrl = _ref.baseUrl,
      baseUrl = _ref$baseUrl === undefined ? DEFAULT_BASE_URL : _ref$baseUrl;

  if (!apiToken) {
    throw new Error('Invalid api token');
  }

  var request = _requestPromise2.default.defaults({
    baseUrl: baseUrl,
    headers: {
      'Api-Token': apiToken
    },
    json: true
  });

  app.use('/sendbird-users', (0, _users2.default)(request));
  app.use('/sendbird-groupChannels', (0, _groupChannels2.default)(request));
  app.use('/sendbird-channelMembers', (0, _channelMembers2.default)(request));
  app.use('/sendbird-messages', (0, _messages2.default)(request));

  return app;
}

module.exports = { init: init };