'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

exports.default = init;

var _feathersErrors = require('feathers-errors');

var _feathersErrors2 = _interopRequireDefault(_feathersErrors);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _camelcaseKeys = require('camelcase-keys');

var _camelcaseKeys2 = _interopRequireDefault(_camelcaseKeys);

var _snakecaseKeys = require('snakecase-keys');

var _snakecaseKeys2 = _interopRequireDefault(_snakecaseKeys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var debug = require('debug')('feathers-sendbird.channelMembers');

var validKeys = ['name', 'coverUrl', 'customType', 'data', 'isDistinct', 'userIds'];
var queryParamKeys = ['token', 'limit', 'showMember', 'showReadReceipt', 'distinctMode', 'membersExactlyIn', 'membersIncludeIn', 'membersNicknameContains', 'queryType', 'customType', 'channelUrls'];

var Service = function () {
  function Service(request) {
    (0, _classCallCheck3.default)(this, Service);
    this.defaults = {
      uri: '/group_channels/{channel_url}/members'
    };

    this.request = request;
  }

  /**
   * https://docs.sendbird.com/platform#group_channel_3_list_members
   * 
   * @param {string} id representing the channel_url
   * @returns 
   * 
   * @memberOf Service
   */


  (0, _createClass3.default)(Service, [{
    key: 'find',
    value: function find() {
      var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      if (params.channelUrl) {
        return _promise2.default.reject(new _feathersErrors2.default.BadRequest('`channelUrl` must be provided'));
      }

      var opts = _lodash2.default.defaults({
        method: 'GET',
        uri: this.defaults.uri.replace('{channel_url}', params.channelUrl)
      }, this.defaults);

      return this.request(opts).then(function (res) {
        debugger;
        var members = res.members || [];
        debug('Loaded %d Channel Members (%s)', members.length, res.next ? 'With More' : 'No More');
        return (0, _camelcaseKeys2.default)(res, { deep: true });
      }).catch(function (err) {
        debug('Request Failed: ', err);
        throw new _feathersErrors2.default.GeneralError('Unknown Error', err);
      });
    }
  }, {
    key: 'get',
    value: function get(id) {
      var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      if (!id) {
        return _promise2.default.reject(new _feathersErrors2.default.BadRequest('`id` needs to be provided'));
      }
      if (!params.channelUrl) {
        return _promise2.default.reject(new _feathersErrors2.default.BadRequest('`channelUrl` needs to be provided'));
      }

      var opts = _lodash2.default.defaults({
        method: 'GET',
        uri: (this.defaults.uri + '/' + id).replace('{channel_url}', params.channelUrl)
      }, this.defaults);

      var intRes = void 0;

      return this.request(opts).then(function (res) {
        intRes = res;
        debug('User [%s] %s A Member of the Group', id, res.is_member ? 'IS' : 'IS NOT');
        return (0, _camelcaseKeys2.default)(res);
      }).catch(function (err) {
        debug('Request Failed: ', err);
        if (/not found/i.test(err.message)) {
          throw new _feathersErrors2.default.NotFound('Channel Member Not Found', { err: err });
        }

        throw new _feathersErrors2.default.GeneralError('Unknown Error', { err: err, msg: err.message });
      });
    }
  }, {
    key: 'create',
    value: function create(data) {
      var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      if (!params.channelUrl) {
        return _promise2.default.reject(new _feathersErrors2.default.BadRequest('`channelUrl` needs to be provided'));
      }

      var userIds = data.userIds || [data.userId];

      if (_lodash2.default.isEmpty(userIds)) {
        return _promise2.default.reject(new _feathersErrors2.default.BadRequest('`userIds` must be provided'));
      }

      var opts = _lodash2.default.defaults({
        method: 'POST',
        uri: (this.defaults.uri + '/invite').replace('{channel_url}/members', params.channelUrl),
        body: { user_ids: userIds }
      }, this.defaults);

      return this.request(opts).then(function (res) {
        debug('Added Members to Channel', res);
        _lodash2.default.each(userIds, function (userId) {
          return console.assert(_lodash2.default.find(res.members, { user_id: userId }));
        });
        return (0, _camelcaseKeys2.default)(res);
      }).catch(function (err) {
        debug('Request Failed: ', err);
        throw new _feathersErrors2.default.GeneralError('Unknown Error', err);
      });
    }
  }]);
  return Service;
}();

function init(request) {
  return new Service(request);
}

init.Service = Service;