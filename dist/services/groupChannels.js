'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

exports.default = init;

var _feathersErrors = require('feathers-errors');

var _feathersErrors2 = _interopRequireDefault(_feathersErrors);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _camelcaseKeys = require('camelcase-keys');

var _camelcaseKeys2 = _interopRequireDefault(_camelcaseKeys);

var _snakecaseKeys = require('snakecase-keys');

var _snakecaseKeys2 = _interopRequireDefault(_snakecaseKeys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var debug = require('debug')('feathers-sendbird.groupChannels');

var validKeys = ['name', 'coverUrl', 'customType', 'data', 'isDistinct', 'userIds'];
var queryParamKeys = ['token', 'limit', 'showMember', 'showReadReceipt', 'distinctMode', 'membersExactlyIn', 'membersIncludeIn', 'membersNicknameContains', 'queryType', 'customType', 'channelUrls'];

var Service = function () {
  function Service(request) {
    (0, _classCallCheck3.default)(this, Service);
    this.defaults = {
      uri: '/group_channels'
    };

    this.request = request;
  }

  /**
   * https://docs.sendbird.com/platform#group_channel_3_list_channels
   * 
   * @param {any} [params={}] 
   * @key token {string}
   * @key limit {string}
   * @key showMember {string}
   * @key showReadReceipt {string}
   * @key distinctMode {string}
   * @key membersExactlyIn {string}
   * @key membersIncludeIn {string}
   * @key membersNicknameContains {string}
   * @key queryType {string}
   * @key customType {string}
   * @key channelUrls {string}
   * 
   * @returns 
   * 
   * @memberOf Service
   */


  (0, _createClass3.default)(Service, [{
    key: 'find',
    value: function find() {
      var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var qs = _lodash2.default.pick(params, queryParamKeys);
      var opts = _lodash2.default.defaults({
        method: 'GET',
        qs: (0, _snakecaseKeys2.default)(qs)
      }, this.defaults);

      return this.request(opts).then(function (res) {
        debug('Loaded %d Group Chats (%s)', res.channels.length, res.next ? 'With More' : 'No More');
        // TODO: Determine why response has nested `channel` objects for each channel in channels
        var response = _lodash2.default.extend({}, res);
        response.channels = _lodash2.default.map(res.channels, function (channel) {
          delete channel.channel;
          channel.channel_type = 'group_channels';
        });
        return (0, _camelcaseKeys2.default)(res, { deep: true });
      }).catch(function (err) {
        debug('Request Failed: ', err);
        throw new _feathersErrors2.default.BadRequest('Unknown Error', err);
      });
    }

    /**
     * https://docs.sendbird.com/platform#group_channel_3_view_a_channel
     * 
     * @param {any} id representing the channel_url
     * @returns 
     * 
     * @memberOf Service
     */

  }, {
    key: 'get',
    value: function get(id) {
      if (!id) {
        return _promise2.default.reject(new _feathersErrors2.default.BadRequest('`id` needs to be provided'));
      }

      var opts = _lodash2.default.defaults({
        method: 'GET',
        uri: this.defaults.uri + '/' + id
      }, this.defaults);

      return this.request(opts).then(function (res) {
        debug('Loaded Group Channel [%s]', id, res);
        res.channel_type = 'group_channels';
        return (0, _camelcaseKeys2.default)(res);
      }).catch(function (err) {
        debug('Request Failed: ', err);
        if (/not found/i.test(err.message)) {
          throw new _feathersErrors2.default.NotFound('Channel Not Found', { err: err });
        }

        throw new _feathersErrors2.default.GeneralError('Unknown Error', err);
      });
    }

    /**
     * https://docs.sendbird.com/platform#group_channel_3_create_a_channel
     * 
     * @param {any} data object, containing the following keys:
     * @key name {String} 
     * @key coverUrl {String} 
     * @key customType {String} 
     * @key data {String} 
     * @key isDistinct {Boolean} 
     * @key userIds {Array} 
     * 
     * @returns {Promise}
     * 
     * @memberOf Service
     */

  }, {
    key: 'create',
    value: function create(data) {
      var body = _lodash2.default.pick(data, validKeys);

      var opts = _lodash2.default.defaults({
        method: 'POST',
        body: (0, _snakecaseKeys2.default)(body)
      }, this.defaults);

      return this.request(opts).then(function (res) {
        debug('Created Group Channel [%s]', res.user_id, res);
        res.channel_type = 'group_channels';
        return (0, _camelcaseKeys2.default)(res);
      }).catch(function (err) {
        debug('Request Failed: ', err);
        throw new _feathersErrors2.default.GeneralError('Unknown Error', err);
      });
    }

    /**
     * https://docs.sendbird.com/platform#group_channel_3_update_a_channel
     * 
     * @param {any} id 
     * @param {any} query object, containing the following keys:
     * @key name {String} 
     * @key coverUrl {String} 
     * @key customType {String} 
     * @key data {String} 
     * @key isDistinct {Boolean} 
     * @returns {Promise}
     * 
     * @memberOf Service
     */

  }, {
    key: 'update',
    value: function update(id, data) {
      var body = _lodash2.default.pick(data, validKeys);

      if (body.userIds) {
        debug('Channel members can not be changed here. Use "Invite Users"');
        delete body.userIds;
      }

      if (_lodash2.default.isEmpty(body)) {
        return _promise2.default.reject(new _feathersErrors2.default.BadRequest('No Properties to Update for User'));
      }

      var opts = _lodash2.default.defaults({
        method: 'PUT',
        body: (0, _snakecaseKeys2.default)(body)
      }, this.defaults);

      return this.request(opts).then(function (res) {
        debug('Updated Group Channel [%s]', res.user_id, res);
        res.channel_type = 'group_channels';
        return (0, _camelcaseKeys2.default)(res);
      }).catch(function (err) {
        debug('Request Failed: ', err);
        throw new _feathersErrors2.default.GeneralError('Unknown Error', err);
      });
    }
  }]);
  return Service;
}();

function init(request) {
  return new Service(request);
}

init.Service = Service;