'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

exports.default = init;

var _feathersErrors = require('feathers-errors');

var _feathersErrors2 = _interopRequireDefault(_feathersErrors);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _camelcaseKeys = require('camelcase-keys');

var _camelcaseKeys2 = _interopRequireDefault(_camelcaseKeys);

var _snakecaseKeys = require('snakecase-keys');

var _snakecaseKeys2 = _interopRequireDefault(_snakecaseKeys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var debug = require('debug')('feathers-sendbird.messages');

var Service = function () {
  function Service(request) {
    var _this = this;

    (0, _classCallCheck3.default)(this, Service);
    this.defaults = {
      uri: '/{channel_type}/{channel_url}/messages'
    };

    this.request = request;

    this.getUri = function (type, channelUrl) {
      return _this.defaults.uri.replace('{channel_type}', type).replace('{channel_url}', channelUrl);
    };
  }

  (0, _createClass3.default)(Service, [{
    key: 'find',
    value: function find() {
      var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      if (_lodash2.default.isEmpty(params.channelUrl)) {
        return _promise2.default.reject(new _feathersErrors2.default.BadRequest('`channelUrl` needs to be provided'));
      }
      if (_lodash2.default.isEmpty(params.messageTs)) {
        return _promise2.default.reject(new _feathersErrors2.default.BadRequest('`messageTs` must be provided'));
      }

      _lodash2.default.defaults(params, { channelType: 'group_channels' });

      var qs = _lodash2.default.pick(params, ['messageTs', 'prevLimit', 'nextLimit', 'include', 'reverse', 'customType', 'messageType', 'senderId']);
      var opts = _lodash2.default.defaults({
        method: 'GET',
        uri: this.getUri(params.channelType, params.channelUrl),
        qs: (0, _snakecaseKeys2.default)(qs)
      }, this.defaults);

      return this.request(opts).then(function (res) {
        debug('Loaded %d Users (%s)', res.users.length, res.next ? 'With More' : 'No More');
        return (0, _camelcaseKeys2.default)(res, { deep: true });
      }).catch(function (err) {
        debug('Request Failed: ', err);
        throw new _feathersErrors2.default.BadRequest('Unknown Error', err);
      });
    }
  }, {
    key: 'create',
    value: function create(data) {
      var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      _lodash2.default.defaults(params, {
        channelType: _lodash2.default.get(params, 'channel.channelType', 'group_channels'),
        channelUrl: _lodash2.default.get(params, 'channel.channelUrl')
      });

      if (_lodash2.default.isEmpty(params.channelUrl)) {
        return _promise2.default.reject(new _feathersErrors2.default.BadRequest('`channel` object, or `channelUrl` must be provided in params'));
      }

      var body = _lodash2.default.defaults(data, {
        messageType: 'MESG',
        customType: null,
        data: null,
        markAsRead: true
      });

      if (body.messageType === 'MESG' && _lodash2.default.isEmpty(body.message)) {
        return _promise2.default.reject(new _feathersErrors2.default.BadRequest('Must include Message Text'));
      }

      var opts = _lodash2.default.defaults({
        method: 'POST',
        body: (0, _snakecaseKeys2.default)(body),
        uri: this.getUri(params.channelType, params.channelUrl)
      }, this.defaults);

      return this.request(opts).then(function (res) {
        debug('Created User [%s]', res.user_id, res);
        return (0, _camelcaseKeys2.default)(res);
      }).catch(function (err) {
        debug('Request Failed: ', err);
        throw new _feathersErrors2.default.GeneralError('Unknown Error', err);
      });
    }
  }]);
  return Service;
}();

function init(request) {
  return new Service(request);
}

init.Service = Service;