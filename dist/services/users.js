'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

exports.default = init;

var _feathersErrors = require('feathers-errors');

var _feathersErrors2 = _interopRequireDefault(_feathersErrors);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _camelcaseKeys = require('camelcase-keys');

var _camelcaseKeys2 = _interopRequireDefault(_camelcaseKeys);

var _snakecaseKeys = require('snakecase-keys');

var _snakecaseKeys2 = _interopRequireDefault(_snakecaseKeys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var debug = require('debug')('feathers-sendbird.users');

var Service = function () {
  function Service(request) {
    (0, _classCallCheck3.default)(this, Service);
    this.defaults = {
      uri: '/users'
    };

    this.request = request;
  }

  // https://docs.sendbird.com/platform#user_3_list_users


  (0, _createClass3.default)(Service, [{
    key: 'find',
    value: function find() {
      var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var qs = _lodash2.default.pick(params, ['token', 'limit', 'activeMode', 'showBot', 'userIds']);
      var opts = _lodash2.default.defaults({
        method: 'GET',
        qs: (0, _snakecaseKeys2.default)(qs)
      }, this.defaults);

      return this.request(opts).then(function (res) {
        debug('Loaded %d Users (%s)', res.users.length, res.next ? 'With More' : 'No More');
        return (0, _camelcaseKeys2.default)(res, { deep: true });
      }).catch(function (err) {
        debug('Request Failed: ', err);
        throw new _feathersErrors2.default.BadRequest('Unknown Error', err);
      });
    }
  }, {
    key: 'get',
    value: function get(id) {
      if (!id) {
        return _promise2.default.reject(new _feathersErrors2.default.BadRequest('`id` needs to be provided'));
      }

      var opts = _lodash2.default.defaults({
        method: 'GET',
        uri: this.defaults.uri + '/' + id
      }, this.defaults);

      return this.request(opts).then(function (res) {
        debug('Loaded User [%s]', id, res);
        return (0, _camelcaseKeys2.default)(res);
      }).catch(function (err) {
        debug('Request Failed: ', err);
        if (/user not found/i.test(err.message)) {
          throw new _feathersErrors2.default.NotFound('User Not Found', { err: err });
        }

        throw new _feathersErrors2.default.GeneralError('Unknown Error', err);
      });
    }
  }, {
    key: 'create',
    value: function create(data) {
      var body = {
        user_id: data.uuid,
        nickname: data.firstName,
        profile_url: data.profileImageURL
      };

      var opts = _lodash2.default.defaults({
        method: 'POST',
        body: body
      }, this.defaults);

      return this.request(opts).then(function (res) {
        debug('Created User [%s]', res.user_id, res);
        return (0, _camelcaseKeys2.default)(res);
      }).catch(function (err) {
        debug('Request Failed: ', err);
        throw new _feathersErrors2.default.GeneralError('Unknown Error', err);
      });
    }
  }, {
    key: 'update',
    value: function update(id, data) {
      var body = {};

      if (data.firstName) {
        body.nickname = data.firstName;
      }
      if (data.profileImageURL) {
        body.profile_url = data.profileImageURL;
      }
      if (!_lodash2.default.isUndefined(data.isDeleted)) {
        body.is_active = !data.isDeleted;
      }

      if (_lodash2.default.isEmpty(body)) {
        return _promise2.default.reject(new _feathersErrors2.default.BadRequest('No Properties to Update for User'));
      }

      var opts = _lodash2.default.defaults({
        method: 'PUT',
        body: (0, _snakecaseKeys2.default)(body)
      }, this.defaults);

      return this.request(opts).then(function (res) {
        debug('Updated User [%s]', res.user_id, res);
        return (0, _camelcaseKeys2.default)(res);
      }).catch(function (err) {
        debug('Request Failed: ', err);
        throw new _feathersErrors2.default.GeneralError('Unknown Error', err);
      });
    }
  }]);
  return Service;
}();

function init(request) {
  return new Service(request);
}

init.Service = Service;