import rp from 'request-promise';

import usersService from './services/users';
import groupChannelsService from './services/groupChannels';
import channelMembers from './services/channelMembers';
import messagesService from './services/messages';

const DEFAULT_BASE_URL = 'https://api.sendbird.com/v3/';

function init(app, { apiToken, baseUrl = DEFAULT_BASE_URL }) {
  if (!apiToken) {
    throw new Error('Invalid api token');
  }
    
  const request = rp.defaults({
    baseUrl,
    headers: {
      'Api-Token': apiToken,
    },
    json: true,
  });

  app.use('/sendbird-users', usersService(request));
  app.use('/sendbird-groupChannels', groupChannelsService(request));  
  app.use('/sendbird-channelMembers', channelMembers(request));
  app.use('/sendbird-messages', messagesService(request));  

  return app;
}

module.exports = { init }
