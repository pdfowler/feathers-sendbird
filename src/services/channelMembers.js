import errors from 'feathers-errors';
import _ from 'lodash';

import camelCaseKeys from 'camelcase-keys';
import snakeCaseKeys from 'snakecase-keys';

const debug = require('debug')('feathers-sendbird.channelMembers');

const validKeys = ['name', 'coverUrl', 'customType', 'data', 'isDistinct', 'userIds'];
const queryParamKeys = ['token', 'limit', 'showMember', 'showReadReceipt', 'distinctMode', 'membersExactlyIn', 'membersIncludeIn', 'membersNicknameContains', 'queryType', 'customType', 'channelUrls', ];
class Service {
  defaults = {
    uri: '/group_channels/{channel_url}/members',
  };

  constructor(request) {
    this.request = request;
  }

  /**
   * https://docs.sendbird.com/platform#group_channel_3_list_members
   * 
   * @param {string} id representing the channel_url
   * @returns 
   * 
   * @memberOf Service
   */
  find(params = {}) {
    if (params.channelUrl) {
      return Promise.reject(new errors.BadRequest('`channelUrl` must be provided'));
    }

    const opts = _.defaults({
      method: 'GET',
      uri: this.defaults.uri.replace('{channel_url}', params.channelUrl),
    }, this.defaults);

    return this.request(opts)
      .then((res) => {
        debugger;
        const members = res.members || [];
        debug('Loaded %d Channel Members (%s)', 
          members.length, res.next ? 'With More' : 'No More');
        return camelCaseKeys(res, {deep: true});
      })
      .catch((err) => {
        debug('Request Failed: ', err);
        throw new errors.GeneralError('Unknown Error', err);
      });
  }

  get(id, params = {}) {
    if (!id) { return Promise.reject(new errors.BadRequest('`id` needs to be provided')); }
    if (!params.channelUrl) { return Promise.reject(new errors.BadRequest('`channelUrl` needs to be provided')); }

    const opts = _.defaults({
      method: 'GET',
      uri: `${this.defaults.uri}/${id}`.replace('{channel_url}', params.channelUrl),
    }, this.defaults);

    let intRes;

    return this.request(opts)
      .then((res) => {
        intRes = res;
        debug('User [%s] %s A Member of the Group', 
          id, res.is_member ? 'IS' : 'IS NOT');
        return camelCaseKeys(res);
      })
      .catch((err) => {
        debug('Request Failed: ', err);
        if (/not found/i.test(err.message)) {
          throw new errors.NotFound('Channel Member Not Found', { err });
        }
          
        throw new errors.GeneralError('Unknown Error', { err, msg: err.message });
      });
  }

  create(data, params = {}) {
    if (!params.channelUrl) { return Promise.reject(new errors.BadRequest('`channelUrl` needs to be provided')); }

    const userIds = data.userIds || [ data.userId ];

    if (_.isEmpty(userIds)) { 
      return Promise.reject(new errors.BadRequest('`userIds` must be provided')); 
    }

    const opts = _.defaults({
      method: 'POST',
      uri: `${this.defaults.uri}/invite`.replace('{channel_url}/members', params.channelUrl),
      body: { user_ids : userIds },
    }, this.defaults);

    return this.request(opts)
      .then((res) => {
        debug('Added Members to Channel', res);
        _.each(userIds, userId => console.assert(_.find(res.members, { user_id : userId })));
        return camelCaseKeys(res);
      })
      .catch((err) => {
        debug('Request Failed: ', err);
        throw new errors.GeneralError('Unknown Error', err);
      });
  }
}

export default function init(request) {
  return new Service(request);
}

init.Service = Service;
