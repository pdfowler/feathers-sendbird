import errors from 'feathers-errors';
import _ from 'lodash';

import camelCaseKeys from 'camelcase-keys';
import snakeCaseKeys from 'snakecase-keys';

const debug = require('debug')('feathers-sendbird.groupChannels');

const validKeys = ['name', 'coverUrl', 'customType', 'data', 'isDistinct', 'userIds'];
const queryParamKeys = ['token', 'limit', 'showMember', 'showReadReceipt', 'distinctMode', 'membersExactlyIn', 'membersIncludeIn', 'membersNicknameContains', 'queryType', 'customType', 'channelUrls', ];
class Service {
  defaults = {
    uri: '/group_channels',
  };

  constructor(request) {
    this.request = request;
  }

  /**
   * https://docs.sendbird.com/platform#group_channel_3_list_channels
   * 
   * @param {any} [params={}] 
   * @key token {string}
   * @key limit {string}
   * @key showMember {string}
   * @key showReadReceipt {string}
   * @key distinctMode {string}
   * @key membersExactlyIn {string}
   * @key membersIncludeIn {string}
   * @key membersNicknameContains {string}
   * @key queryType {string}
   * @key customType {string}
   * @key channelUrls {string}
   * 
   * @returns 
   * 
   * @memberOf Service
   */
  find(params = {}) {
    const qs = _.pick(params, queryParamKeys);
    const opts = _.defaults({
      method: 'GET',
      qs: snakeCaseKeys(qs),
    }, this.defaults);

    return this.request(opts)
      .then((res) => {
        debug('Loaded %d Group Chats (%s)', res.channels.length, res.next ? 'With More' : 'No More');
        // TODO: Determine why response has nested `channel` objects for each channel in channels
        const response = _.extend({}, res);
        response.channels = _.map(res.channels, (channel) => {
          delete channel.channel;
          channel.channel_type = 'group_channels';
        })
        return camelCaseKeys(res, {deep: true});
      })
      .catch((err) => {
        debug('Request Failed: ', err);
        throw new errors.BadRequest('Unknown Error', err);
      });
  }

  /**
   * https://docs.sendbird.com/platform#group_channel_3_view_a_channel
   * 
   * @param {any} id representing the channel_url
   * @returns 
   * 
   * @memberOf Service
   */
  get(id) {
    if (!id) {
      return Promise.reject(new errors.BadRequest('`id` needs to be provided'));
    }

    const opts = _.defaults({
      method: 'GET',
      uri: `${this.defaults.uri}/${id}`,
    }, this.defaults);

    return this.request(opts)
      .then((res) => {
        debug('Loaded Group Channel [%s]', id, res);
        res.channel_type = 'group_channels';
        return camelCaseKeys(res);
      })
      .catch((err) => {
        debug('Request Failed: ', err);
        if (/not found/i.test(err.message)) {
          throw new errors.NotFound('Channel Not Found', { err });
        }
          
        throw new errors.GeneralError('Unknown Error', err);
      });
  }

  /**
   * https://docs.sendbird.com/platform#group_channel_3_create_a_channel
   * 
   * @param {any} data object, containing the following keys:
   * @key name {String} 
   * @key coverUrl {String} 
   * @key customType {String} 
   * @key data {String} 
   * @key isDistinct {Boolean} 
   * @key userIds {Array} 
   * 
   * @returns {Promise}
   * 
   * @memberOf Service
   */
  create(data) {
    const body = _.pick(data, validKeys);

    const opts = _.defaults({
      method: 'POST',
      body: snakeCaseKeys(body),
    }, this.defaults);

    return this.request(opts)
      .then((res) => {
        debug('Created Group Channel [%s]', res.user_id, res);
        res.channel_type = 'group_channels';
        return camelCaseKeys(res);
      })
      .catch((err) => {
        debug('Request Failed: ', err);
        throw new errors.GeneralError('Unknown Error', err);
      });
  }

  /**
   * https://docs.sendbird.com/platform#group_channel_3_update_a_channel
   * 
   * @param {any} id 
   * @param {any} query object, containing the following keys:
   * @key name {String} 
   * @key coverUrl {String} 
   * @key customType {String} 
   * @key data {String} 
   * @key isDistinct {Boolean} 
   * @returns {Promise}
   * 
   * @memberOf Service
   */
  update(id, data) {
    const body = _.pick(data, validKeys);

    if (body.userIds) {
      debug('Channel members can not be changed here. Use "Invite Users"');
      delete body.userIds;
    }

    if (_.isEmpty(body)) {
      return Promise.reject(new errors.BadRequest('No Properties to Update for User'));
    }

    const opts = _.defaults({
      method: 'PUT',
      body: snakeCaseKeys(body),
    }, this.defaults);

    return this.request(opts)
      .then((res) => {
        debug('Updated Group Channel [%s]', res.user_id, res);
        res.channel_type = 'group_channels';
        return camelCaseKeys(res);
      })
      .catch((err) => {
        debug('Request Failed: ', err);
        throw new errors.GeneralError('Unknown Error', err);
      });
  }
}

export default function init(request) {
  return new Service(request);
}

init.Service = Service;
