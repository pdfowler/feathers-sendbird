import errors from 'feathers-errors';
import _ from 'lodash';

import camelCaseKeys from 'camelcase-keys';
import snakeCaseKeys from 'snakecase-keys';

const debug = require('debug')('feathers-sendbird.messages');

class Service {
  defaults = {
    uri: '/{channel_type}/{channel_url}/messages',
  };

  constructor(request) {
    this.request = request;

    this.getUri = (type, channelUrl) => {
      return this.defaults.uri.replace('{channel_type}', type).replace('{channel_url}', channelUrl);
    };
  }

  find(params = {}) {
    if (_.isEmpty(params.channelUrl)) {
      return Promise.reject(new errors.BadRequest('`channelUrl` needs to be provided'));
    }
    if (_.isEmpty(params.messageTs)) {
      return Promise.reject(new errors.BadRequest('`messageTs` must be provided'));
    }

    _.defaults(params, { channelType : 'group_channels' });

    const qs = _.pick(params, ['messageTs', 'prevLimit', 'nextLimit', 'include', 'reverse', 'customType', 'messageType', 'senderId']);
    const opts = _.defaults({
      method: 'GET',
      uri: this.getUri(params.channelType, params.channelUrl),
      qs: snakeCaseKeys(qs),
    }, this.defaults);

    return this.request(opts)
      .then((res) => {
        debug('Loaded %d Users (%s)', res.users.length, res.next ? 'With More' : 'No More');
        return camelCaseKeys(res, {deep: true});
      })
      .catch((err) => {
        debug('Request Failed: ', err);
        throw new errors.BadRequest('Unknown Error', err);
      });
  }

  create(data, params = {}) {
    _.defaults(params, {
      channelType: _.get(params, 'channel.channelType', 'group_channels'),
      channelUrl: _.get(params, 'channel.channelUrl'),
    });

    if (_.isEmpty(params.channelUrl)) {
      return Promise.reject(new errors.BadRequest('`channel` object, or `channelUrl` must be provided in params'));
    }
    
    const body = _.defaults(data, {
      messageType: 'MESG',
      customType: null,
      data: null,
      markAsRead: true,
    });

    if (body.messageType === 'MESG' && _.isEmpty(body.message)) {
      return Promise.reject(new errors.BadRequest('Must include Message Text'));
    }

    const opts = _.defaults({
      method: 'POST',
      body: snakeCaseKeys(body),
      uri: this.getUri(params.channelType, params.channelUrl),
    }, this.defaults);

    return this.request(opts)
      .then((res) => {
        debug('Created User [%s]', res.user_id, res);
        return camelCaseKeys(res);
      })
      .catch((err) => {
        debug('Request Failed: ', err);
        throw new errors.GeneralError('Unknown Error', err);
      });
  }
}

export default function init(request) {
  return new Service(request);
}

init.Service = Service;
