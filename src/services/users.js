import errors from 'feathers-errors';
import _ from 'lodash';

import camelCaseKeys from 'camelcase-keys';
import snakeCaseKeys from 'snakecase-keys';

const debug = require('debug')('feathers-sendbird.users');

class Service {
  defaults = {
    uri: '/users',
  };

  constructor(request) {
    this.request = request;
  }

  // https://docs.sendbird.com/platform#user_3_list_users
  find(params = {}) {
    const qs = _.pick(params, ['token', 'limit', 'activeMode', 'showBot', 'userIds']);
    const opts = _.defaults({
      method: 'GET',
      qs: snakeCaseKeys(qs),
    }, this.defaults);

    return this.request(opts)
      .then((res) => {
        debug('Loaded %d Users (%s)', res.users.length, res.next ? 'With More' : 'No More');
        return camelCaseKeys(res, {deep: true});
      })
      .catch((err) => {
        debug('Request Failed: ', err);
        throw new errors.BadRequest('Unknown Error', err);
      });
  }

  get(id) {
    if (!id) {
      return Promise.reject(new errors.BadRequest('`id` needs to be provided'));
    }

    const opts = _.defaults({
      method: 'GET',
      uri: `${this.defaults.uri}/${id}`,
    }, this.defaults);

    return this.request(opts)
      .then((res) => {
        debug('Loaded User [%s]', id, res);
        return camelCaseKeys(res);
      })
      .catch((err) => {
        debug('Request Failed: ', err);
        if (/user not found/i.test(err.message)) {
          throw new errors.NotFound('User Not Found', { err });
        }
          
        throw new errors.GeneralError('Unknown Error', err);
      });
  }

  create(data) {
    const body = {
      user_id: data.uuid,
      nickname: data.firstName,
      profile_url: data.profileImageURL,
    };

    const opts = _.defaults({
      method: 'POST',
      body,
    }, this.defaults);

    return this.request(opts)
      .then((res) => {
        debug('Created User [%s]', res.user_id, res);
        return camelCaseKeys(res);
      })
      .catch((err) => {
        debug('Request Failed: ', err);
        throw new errors.GeneralError('Unknown Error', err);
      });
  }

  update(id, data) {
    const body = {};

    if (data.firstName) { body.nickname = data.firstName; }
    if (data.profileImageURL) { body.profile_url = data.profileImageURL; }
    if (!_.isUndefined(data.isDeleted)) { body.is_active = !data.isDeleted; }
    
    if (_.isEmpty(body)) {
      return Promise.reject(new errors.BadRequest('No Properties to Update for User'));
    }

    const opts = _.defaults({
      method: 'PUT',
      body : snakeCaseKeys(body),
    }, this.defaults);

    return this.request(opts)
      .then((res) => {
        debug('Updated User [%s]', res.user_id, res);
        return camelCaseKeys(res);
      })
      .catch((err) => {
        debug('Request Failed: ', err);
        throw new errors.GeneralError('Unknown Error', err);
      });
  }
}

export default function init(request) {
  return new Service(request);
}

init.Service = Service;
